package DataBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DataBase{
    private Connection con = null;
    public DataBase(String db, String user, String pass){
        try{
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + db, user, pass);
        }
        catch(SQLException e){
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }   
    }

    public static DataBase getDbObject(){
        return new DataBase("LMS", "postgres", "sam123");
    }

    public void executeQuery(String sql){
        try {
            Statement query = con.createStatement();
            query.executeUpdate(sql);
        } catch (SQLException e){
            e.printStackTrace();
        }
    }

    public ResultSet select(String sql){
        ResultSet data = null;
        try{
            Statement query = con.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            data = query.executeQuery(sql);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return data;  
    }

    public Integer Len(ResultSet data){
        int rowCount = 0;
        try {
            if (data.last()){
              rowCount = data.getRow();
              data.beforeFirst();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowCount;
    }

    public void close() throws SQLException{
        con.close();
    }
}   