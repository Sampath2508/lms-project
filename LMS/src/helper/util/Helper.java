package helper.util;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class Helper {
    public static Scanner input = new Scanner(System.in);

    public static String InputStr(String display) throws NoSuchElementException{
        System.out.print(display);
        return input.nextLine();
    }

    public static Integer InputInt(String display) throws InputMismatchException, NoSuchElementException{
            try{
                return Integer.parseInt(InputStr(display));
            }catch(Exception e){
                throw new InputMismatchException("Invalid Input");
            }
            
             
    }

    public static Integer Int(String num) throws NumberFormatException{
        return Integer.parseInt(num);
    }

    public static Long findDateDifference(String start_date, String end_date) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        long difference_In_Days = -1;
        long difference_In_Time = sdf.parse(end_date).getTime() - sdf.parse(start_date).getTime();
        difference_In_Days = (difference_In_Time / (1000 * 60 * 60 * 24)) % 365;
        return difference_In_Days;
    }

    public static void printError(Exception e){
        for(int i = 0;i < e.getStackTrace().length; i++){
            StackTraceElement err = e.getStackTrace()[i];
            System.out.println(MessageFormat.format("Error | {0} | Message : {1} | Line : {2} | Class Name : {3} | Method Name : {4}", 
                           e.getClass(), e.getLocalizedMessage(), err.getLineNumber(), err.getClassName(), err.getMethodName()));
        }
    }

}
