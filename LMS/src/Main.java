import java.sql.ResultSet;
import java.sql.SQLException;

import DataBase.DataBase;
import book.BookStore;

import users.Librarian;
import users.Users;
import helper.util.Helper;

class Main{    
    public static Integer userType = - 1;
    public static DataBase db = DataBase.getDbObject();
    public static Users users = new Users();
    public static BookStore book = null;

    public static void login(){
        try{    
            String userName = Helper.InputStr("Enter UserName : ");
            String password = Helper.InputStr("Enter Password : ");
            ResultSet data = db.select(String.format("select t.usertype, u.userid from users as u"
                                                     + "inner join usertype as ut on u.userid = ut.userid"
                                                     + "inner join type as t on ut.typeid = t.id"
                                                     + " where u.username = '%s' and u.password = '%s';", userName, password));

                if (db.Len(data) == 1){
                    data.next();
                    if(data.getString("usertype").equals("student")){
                        userType = 1;
                    }
                    else if(data.getString("usertype").equals("teacher")){
                        userType = 2;
                    }else{
                        userType = 3;
                    }
                    users.setId(data.getInt("userid"));
                    users.setUserName(userName);
                    System.out.println("Successfully loged in...");
                    System.out.println("Welcome " + userName);
                }else{
                    System.out.println("Invalid Username or Password");
                }
        }catch(Exception e){
            Helper.printError(e);
        }
        
    }

    public static void logout(){     
        users.setId(-1);
        users.setUserName(null);   
        userType = -1;
        System.out.println("Successfully Loged Out...");
    }

    public static void main(String[] arg){
            
            while(true){
                Integer num = -1;
                try{
                    num = Helper.InputInt("* Enter 1 for Login\n* Enter 2 for Exit\nEnter Number : ");
                }catch(Exception e){Helper.printError(e);}
                if (num == 1){
                    login(); 
                    book = new BookStore();
                    if (userType == 1 || userType == 2){
                        while(userType != -1){
                            try{
                                Integer stuNum = Helper.InputInt("* Enter 1 for Renew Book\n* Enter 2 for BorrowedBook\n"
                                                                    + "* Enter 3 for SearchBy Id\n* Enter 4 for SearchBy Book Name\n* Enter 5 for SearchBy Author\n"     
                                                                    + "* Enter 6 for SearchBy Type\n"
                                                                    + "* Enter 7 for Book Info\n"
                                                                    + "* Enter 8 for Get Lastest Edition\n"
                                                                    + "* Enter 9 for Get All Book\n"
                                                                    + "* Enter 10 for PayFine\n* Enter 11 for Logout\nEnter Number between 1 - 11 : ");
                                if (stuNum == 1){
                                    if (userType == 1) users.renewBook(userType);
                                    else throw new Exception("This option is invalid for this user");
                                }else if (stuNum == 2){
                                    users.generateBorrowedBook();
                                }else if (stuNum == 3){
                                    Integer id = Helper.InputInt("Enter Book Id : ");
                                    book.searchBook(id);
                                }else if (stuNum == 4){
                                    String name = Helper.InputStr("Enter Book Name : ");
                                    book.searchBook(name);
                                }else if (stuNum == 5){
                                    String author = Helper.InputStr("Enter Book Author : ");
                                    book.searchByAuthor(author);
                                }else if (stuNum == 6){
                                    String type = Helper.InputStr("Enter Type : ");
                                    book.searchByType(type);
                                }else if (stuNum == 7){
                                    String name = Helper.InputStr("Enter Book Name : ");
                                    book.getBookInfo(name);
                                }else if (stuNum == 8){
                                    book.getLastestEdition();
                                }else if(stuNum == 9){
                                    book.getBooks();
                                }else if (stuNum == 10){
                                    if(userType == 1){
                                        users.payFine();
                                    }else{
                                        throw new Exception("This option is invalid for this user");
                                    }
                                }else if (stuNum == 11){
                                    logout();
                                }else{
                                    System.out.println("Invalid Number : Enter Number Between 1 - 11");
                                }
                            }catch(Exception e){Helper.printError(e);} 
                        }
                    }else if (userType == 3){
                        Librarian lib = new Librarian();
                        while(userType != -1){
                            try{
                                Integer libNum = Helper.InputInt("* Enter 1 for Issue Book\n* Enter 2 for Return Book\n"
                                                                   + "* Enter 3 for Borrowed Books\n* Enter 4 for Student & Teacher Borrowed Book\n"
                                                                   + "* Enter 5 for Add Book & Update Book\n"
                                                                   + "* Enter 6 for Delete Book\n"
                                                                   + "* Enter 7 for SearchBy ID\n"
                                                                   + "* Enter 8 for SearchBy Book Name\n"
                                                                   + "* Enter 9 for SearchBY Author\n"
                                                                   + "* Enter 10 for SearchBy Type\n"
                                                                   + "* Enter 11 for Book Info\n"
                                                                   + "* Enter 12 for Get Lastest Edition\n"
                                                                   + "* Enter 13 for To Get Remaining Copies of Book\n"
                                                                   + "* Enter 14 for View Books\n"
                                                                   + "* Enter 15 for Book History\n"
                                                                   + "* Enter 16 for AddUser\n"
                                                                   + "* Enter 17 for Logout\nEnter Number between 1 - 17 : ");

                                if(libNum == 1){
                                    lib.issueBook();
                                }else if(libNum == 2){
                                    lib.returnBook();
                                }else if(libNum == 3){
                                    users.generateBorrowedBook();
                                }else if(libNum == 4){
                                    lib.getBorrowedBookForUser();
                                }else if(libNum == 5){
                                    book.addBook();
                                }else if(libNum == 6){
                                    book.deleteBook();
                                }else if(libNum == 7){
                                    Integer id = Helper.InputInt("Enter Book Id : ");
                                    book.searchBook(id);
                                }else if(libNum == 8){
                                    String name = Helper.InputStr("Enter Book Name : ");
                                    book.searchBook(name);
                                }else if(libNum == 9){
                                    String author = Helper.InputStr("Enter Book Author : ");
                                    book.searchByAuthor(author);
                                }else if(libNum == 10){
                                    String type = Helper.InputStr("Enter Type : ");
                                    book.searchByType(type);
                                }else if(libNum == 11){
                                    String name = Helper.InputStr("Enter Book Name : ");
                                    book.getBookInfo(name);
                                }else if(libNum == 12){
                                    book.getLastestEdition();
                                }else if(libNum == 13){
                                    book.getRemainingBookCopies();
                                }else if(libNum == 14){
                                    book.getBooks();
                                }else if(libNum == 15){
                                    book.getUserBookHistory();
                                }else if(libNum == 16){
                                    lib.addUser();
                                }else if(libNum == 17){
                                    logout();
                                }
                                else{
                                    System.out.println("Invalid Number : Enter Number between 1 - 16");
                                }
                            }catch(Exception e){
                                Helper.printError(e);
                            }
                        }
                    }
                }else if(num == 2){
                    System.out.println("Exiting...");
                    try {
                        db.close();
                        users.close();
                        book.close();
                    } catch (SQLException e) {
                        Helper.printError(e);
                    }
                    break;
                }
                else{
                    System.out.println("Invalid Number : Enter Number Between 1 - 2");
                }
            }
        }
    }

