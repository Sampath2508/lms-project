package users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import DataBase.DataBase;
import helper.util.Helper;

public class Users{
   protected DataBase db = DataBase.getDbObject();
   private Integer id = -1;
   private String userName = null;

   public Users(){
       
   }

   public void setId(Integer id){
      this.id = id;
   }

   public void setUserName(String userName){
      this.userName = userName;
   }

   public Integer getId(){
      return id;
   }

   public String getUserName(){
      return userName;
   }

   public void generateBorrowedBook() throws SQLException{
      System.out.println("UserName : " + userName);
      System.out.println(id);
      ResultSet result = db.select("select * from borrowbook where userid = " + id + ";");
        if(db.Len(result) == 0){
            System.out.println("No Book Borrowed Yet");
        }else{
            while(result.next()) System.out.println(String.format("UserName : %s\nBook Id : %s\nDue Date : %s", 
                                                    userName, result.getString("bookid"), result.getString("duedate")));                       
        }
   }

//    public HashMap<String, String> getUserInfo(){
//       HashMap<String, String> user = new HashMap<String, String>();
//         ResultSet data = db.select("select * from users where userid = " + id);
        
//         try {
//             while(data.next()){

//                 user.put("id", data.getString("userid"));
//                 user.put("username", data.getString("username"));
//                 user.put("email", data.getString("email"));
//                 user.put("department", data.getString("department"));
//                 user.put("gender", data.getString("gender"));
//             }

//         } catch (SQLException e) {
//             e.printStackTrace();
//         }        
//         return user;
//    }

   public void renewBook(Integer userType) throws Exception, SQLException{
        String bookId = null;
        Date dueDate = null;
        Integer userId = id;
        if(userType == 3){
            userId = Helper.InputInt("Enter User Id : ");
        }
        ResultSet result = db.select(String.format("select * from borrowbook where userid = %s",userId));
        if(db.Len(result) == 0){
            System.out.println("No Books To Renew...");
        }else{
            String isbn = Helper.InputStr("Enter isbn : ");
            while(result.next()){
                if (result.getString("bookid").contains(isbn)) {
                    bookId = result.getString("bookid");
                    dueDate = result.getDate("duedate");
                    break;    
                }
            }

            if(bookId != null){
                if(Helper.findDateDifference(result.getString("borroweddate"), result.getString("duedate")).equals(new Long(10))){
                    Calendar c = Calendar.getInstance();
                    c.setTime(dueDate);
                    if (userType == 1) c.add(Calendar.DATE, 5);
                    else if (userType == 3){ 
                        Integer day = 31;
                        while(day > 30){
                            day = Helper.InputInt("Day Range 1 - 30:\nEnter How Many Day : ");
                            if(day > 30) System.out.println("Invalid Number : Please Enter Number between 1 - 30");
                        }
                        c.add(Calendar.DATE, day);
                    } 
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    db.executeQuery(String.format("UPDATE borrowbook set duedate = '%s' where userid = %s;",sdf.format(c), userId));
                }else{
                    System.out.println("Out Of Duedate...");
                }
            }else{
                System.out.println("No result founded...");
            }
        }
   }

   public void payFine() throws SQLException, Exception{
    Long fine = (long) 0;
    ResultSet result = db.select(String.format("select * from bookhistory where userid = %s", getId()));
    while(result.next()){
        Long day = Helper.findDateDifference(result.getString("duedate"), result.getString("returndate"));
        if (day > 0){
            fine += (day * 10);
        }
    }
    System.out.println("UserName : " + userName + "\nFine : " + fine);
  }

  public void close(){
      try {
        db.close();
    } catch (SQLException e) {
        Helper.printError(e);
    }
  }
}