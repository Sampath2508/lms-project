package users;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import helper.util.Helper;

public class Librarian extends Users{    

    @Override
    @Deprecated
    public void payFine() throws Exception{
            throw new Exception("Access denied");    
    }

    public Integer chooseBook(ResultSet data) throws SQLException{
        Integer len = db.Len(data);
        for(int i = 0; i < len && data.next(); i++){
            System.out.println(String.format("Enter %o for | Book Name : %s | Edition : %s", i, data.getString("name"), data.getString("edition")));
        }
        Integer ind = -1;
        while(ind > len || ind < 0){
            ind = Helper.InputInt("Enter Number between 0 - " + (len - 1) + " : ");
            if(ind > len || ind < 0) System.out.println("Please Enter the Number Between 0 - " + (len - 1));
        }
        return ind;
    }

    public void addUser() throws SQLException, Exception{
        String department = null;
        Integer usertype = Helper.InputInt("* Enter 1 for Student\n* Enter 2 for Teacher\n* Enter 3 for Librarian\nEnter Number : ");
        String name = Helper.InputStr("Enter Your Name : ");
        String password = Helper.InputStr("Enter Password : ");
        String email = Helper.InputStr("Enter Email : ");
        if (usertype != 3) department = Helper.InputStr("Enter Department : ");
        String gender = Helper.InputStr("* Enter M for Male\n* Enter F for Female\nEnter Gender : ");
        if(usertype != 3) db.executeQuery(String.format("insert into users (username, password, email, gender, department) values ('%s', '%s', '%s', '%s', '%s')", name, password, email, gender, department));
        else db.executeQuery(String.format("insert into users (username, password, email, gender) values ('%s', '%s', '%s', '%s', '%s')", name, password, email, gender));
        ResultSet result = db.select("select * from users where email = '" + email +"';");
        result.next();
        
        db.executeQuery(String.format("insert into usertype (userid, typeid) values (%s, %s)", result.getInt("userid"), usertype));
        System.out.println("User Created...");
    }

    public void issueBook() throws SQLException{
        String bookName = Helper.InputStr("Enter Book Name : ");
        Integer edition = Helper.InputInt("Enter Edition : ");

        ResultSet result = db.select("select * from bookstore as bs "
                                    + " inner join bookedition as be on be.bookid = bs.bookid "
                                    + " where lower(bs.name) like '%" + bookName + "%' and be.edition = " + edition +";");
        if(db.Len(result) == 0){
            System.out.println("No Result Founded...");
        }else{
            result.absolute(chooseBook(result));
            result.next();
            Integer remainingCopies = result.getInt("remainingcopies");
            if (remainingCopies == 0) System.out.println("Out of Stocks...");
            else{
                Integer userId = Helper.InputInt("Enter UserId : ");
                ResultSet data = db.select("select * from users where userid = "+userId+";");
                if (db.Len(data) == 1){
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");  
                    Calendar cal = Calendar.getInstance();  
                    cal.setTime(new Date());                           
                    cal.add(Calendar.DAY_OF_MONTH, 10);  
                    String dueDate = sdf.format(cal.getTime()); 
                    ResultSet user = db.select("select * from usertype where userid = " + userId);
                    user.next();
                    if (user.getInt("typeid") > 1) dueDate = null;
                    db.executeQuery("update bookedition set remainingcopies = "+(remainingCopies - 1)+" where isbn = "+result.getInt("isbn")+";");
                    db.executeQuery("insert into borrowbook (userid, bookid, duedate) values ("+userId+", "+result.getInt("isbn")+", '"+dueDate+"');");
                    System.out.println("Book Issued...");
                }else System.out.println("User Doesn't Exist...");             
            }   
        }
    }

    public void getBorrowedBookForUser(){
        ResultSet result = db.select(String.format("select * from borrowbook;"));
        if(db.Len(result) == 0){
            System.out.println("No Book Borrowed Yet");
        }else{
            try {
                while(result.next()){
                    System.out.println(String.format("User Id : %s\nBook Isbn : %s\nDue Date : %s", 
                    result.getInt("userid"), result.getString("bookid"), result.getString("duedate")));                 
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void returnBook() throws SQLException, Exception{   
        Integer userId = Helper.InputInt("Enter User Id : ");
        String bookName = Helper.InputStr("Enter Book Name : ");
        ResultSet result = db.select("select * from bookedition as be "
                                     + "inner join bookstore as bs on be.bookid = bs.bookid "
                                     + "inner join borrowbook as bb on bb.bookid = be.isbn "
                                     + "inner join usertype as ut on bb.userid = ut.userid "
                                     + "where lower(bs.name) like '%" + bookName + "%' and bb.userid = " + userId + ";");
        if(db.Len(result) == 0) System.out.println("Result Not Founded...");
        else{
            if(db.Len(result) == 1){
                result.next();
            }else{
                result.absolute(chooseBook(result));
                result.next();
            }
            Integer isbn = result.getInt("isbn");
            String bDate = result.getString("borroweddate");
            db.executeQuery("delete from borrowbook where bookid = " + isbn +";");
            if (result.getInt("typeid") != 1) db.executeQuery(String.format("insert into bookhistory (userid, bookid, borroweddate, duedate) values (%s, %s, '%s', '%s')", userId, isbn, bDate, null, null));
            else db.executeQuery(String.format("insert into bookhistory (userid, bookid, borroweddate, duedate) values (%s, %s, '%s', '%s')", userId, isbn, bDate, result.getString("duedate")));
            db.executeQuery("update bookedition set remainingcopies = " + (result.getInt("remainingcopies") + 1) + " where isbn = "+isbn+";");
            System.out.println(result.getString("name") + " Book Returned..");
        }
    }
}