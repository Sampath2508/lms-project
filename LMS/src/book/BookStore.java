package book;

import java.sql.ResultSet;
import java.sql.SQLException;

import DataBase.DataBase;
import helper.util.Helper;

public class BookStore{

   private DataBase db = DataBase.getDbObject();


   public void getBooks(){
      ResultSet result = db.select("select * from bookstore;");
      
      try { 
         while(result.next()){
            System.out.println(String.format("Books Id : %s"
                                           + "\nBook Name : %s"
                                           + "\nAuthor : %s", 
                                           result.getInt("bookid"), 
                                           result.getString("name"), 
                                           result.getString("author")));
         }
      } catch (SQLException e){
         e.printStackTrace();
      }
   }

   public void addBook() throws SQLException, Exception{
      Integer num = Helper.InputInt("* Enter 1 for Update book\n* Enter 2 for Add Book\nEnter Number : ");
      if(num == 1){
         Integer isbn = Helper.InputInt("Enter isbn Number : ");
         Integer totalCopies = Helper.InputInt("Enter No.Of.Copies : ");
         db.executeQuery(String.format("update bookedition set totalcopies = %s where isbn = %s", totalCopies, isbn));
      }else if(num == 2){
         String name = Helper.InputStr("Enter Book Name : ");
         String author = Helper.InputStr("Enter Author Name : ");
         
         String type = Helper.InputStr("Enter Book Type Id: ");
         
         db.executeQuery(String.format("insert into bookstore (booktype, name, author) values (%s, '%s', '%s')", Helper.Int(type), name, author));
         
         ResultSet data = db.select(String.format("select bookid from bookstore where name = '%s'", name));
         data.next();
         while(true){
            Integer n = Helper.InputInt("Enter 1 for Add Edition or 2 for exist : ");
            if(n == 1){
               String isbn = Helper.InputStr("Enter Isbn Number : ");
               String year = Helper.InputStr("Enter Published Year : ");
               String edition = Helper.InputStr("Enter Edition : ");
               String totalCopy = Helper.InputStr("Enter Total Book Copy : ");
               db.executeQuery(String.format("insert into bookedition (bookid, isbn, year, edition, totalcopies, remainingcopies) values (%s, %s, %s, %s, %s, %s)", data.getInt("bookid"), isbn, year, edition, totalCopy, totalCopy));
               System.out.println("Book Added...");
            } else{
               break;
            }
         }       
      }
   }

   public void deleteBook() throws Exception{
      Integer bookId = Helper.InputInt("Enter Book Id : ");
      db.executeQuery(String.format("delete from bookedition where bookid = %s", bookId));
      db.executeQuery(String.format("delete from bookstore where bookid = %s", bookId));
      System.out.println("Book Delete...");
   }

   public void searchBook(Integer id) throws Exception{
      ResultSet data = db.select(String.format("select bookid, name, author from bookstore" 
                  + " where bookid = %s ;", id));
      if(db.Len(data) == 0){
         System.out.println("No result founded..");
      }else{
         try {
            while(data.next()){
               System.out.println(String.format("Book Id : %s\nBook Name : %s\nAuthor : %s", data.getInt("bookid"), data.getString("name"), data.getString("author")));
            }
         } catch (SQLException e) {
            e.printStackTrace();
         } 
      }
   }

   public void searchBook(String bName){
      ResultSet data = db.select("select bookid, name, author from bookstore where lower(name) like '%"+bName+"%';");
      if(db.Len(data) == 0){
         System.out.println("No result founded..");
      }else{
         try {
            while(data.next()){
               System.out.println(String.format("Book Id : %s\nBook Name : %s\nAuthor : %s", data.getInt("bookid"), data.getString("name"), data.getString("author")));
            }
         } catch (SQLException e) {
            e.printStackTrace();
         } 
      }
   }

   public void searchByAuthor(String author){
      ResultSet data = db.select("select b.bookid, b.name, b.author ,bt.typename from bookstore as b "
                               + "inner join booktype as bt on bt.typeid = b.booktype "
                               + "where lower(b.author) like '%" + author.toLowerCase() + "%';");
      if(db.Len(data) == 0){
         System.out.println("No result founded..");
      }else{
         try {
            while(data.next()){
               System.out.println(String.format("Book Id : %s\nBook Name : %s\nAuthor : %s", data.getInt("bookid"), 
                                  data.getString("name"), data.getString("author")));
            }
         } catch (SQLException e) {
            e.printStackTrace();
         } 
      }
   }

   public void searchByType(String type){
      ResultSet data = db.select(String.format("select b.bookid, b.name, b.author from bookstore as b "
                                             + "inner join booktype as bt on b.booktype = bt.typeid " 
                                             + "where lower(bt.typename) = '%s';", type.toLowerCase()));
      if(db.Len(data) == 0){
         System.out.println("No result founded..");
      }else{
         try {
            while(data.next()){
               System.out.println(String.format("Type : %s\nBook Id : %s\nBook Name : %s\nAuthor : %s", type, data.getInt("bookid"),
                                  data.getString("name"), data.getString("author")));
            }
         } catch (SQLException e) {
            e.printStackTrace();
         } 
      }
   }

   public void getBookInfo(String name){
      ResultSet data = db.select("select * from bookstore as b"
                                 + "inner join booktype as bt on b.booktype = bt.typeid"
                                 + "inner join bookedition as be on be.bookid = b.bookid"
                                 + "where lower(b.name) like '%" + name.toLowerCase() + "%';");
      if(db.Len(data) == 0){
         System.out.println("No result founded..");
      }else{
         try {
            while(data.next()){
               System.out.println(String.format("Book Id : %s\nIsbn : %s\nBook Type : %s\nBook Name : %s\nAuthor : %s\nYear : %s\nEdition : %s", 
                                 data.getInt("bookid"), data.getInt("isbn"), data.getString("typename"), data.getString("name"), 
                                 data.getString("author"), data.getString("year"), data.getString("edition")));
            }
         } catch (SQLException e) {
            e.printStackTrace();
         } 
      }
   }

   public void getRemainingBookCopies() throws SQLException, Exception{
      Integer isbn = Helper.InputInt("Enter Isbn Number : ");
      ResultSet result = db.select(String.format("select remainingcopies from bookedition where isbn = %s", isbn));
      if(db.Len(result) == 0) System.out.println("No Result is Founded...");
      else{
         result.next();
         System.out.println(result.getInt("remainingcopies"));
      }
   }

   public void getLastestEdition() throws SQLException, Exception{
      Integer bookId = Helper.InputInt("Enter Book Id : ");
      ResultSet result = db.select("select * from bookedition where bookid = "+bookId+" order by edition desc;");
      if(db.Len(result) == 0) System.out.println("No Result Founded...");
      else{
         result.next();
         System.out.println(String.format("Book Isbn : %s\nBook Edition : %s", result.getString("isbn"), 
                                          result.getString("edition")));
      }
   }

   public void getUserBookHistory() throws SQLException, Exception{
      Integer userId = Helper.InputInt("Enter User Id : ");
      ResultSet data = db.select("select * from bookhistory where userid = " + userId + ";");
      System.out.println("User Id : " + userId);
      if (db.Len(data) == 0){
         System.out.println("No Result Founded..");
      }else{
         while(data.next()){
            System.out.println(String.format("Book Id : %s\nBorrowed Date : %s\nDue Date : %s\nReturn Date : %s",
                               data.getString("bookid"), data.getString("borroweddate"), data.getString("duedate"), 
                               data.getString("returndate")));
         }
      }     
   }

   public void close(){
      try {
         db.close();
      } catch (SQLException e) {
         e.printStackTrace();
      }
   }
}